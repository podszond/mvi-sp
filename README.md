# Reinforcement Learning in Recommendation

Recommendation algorithms are typically optimized for the conversion rate.
However many companies are interested in optimizing customer lifetime value
or engagement directly.
Reinforcement learning in recommendation can possibly address this problem.

keywords: recommender systems, reinforcement learning

## Assignment

Survey the state of the art contextual bandit algorithms for recommendation.
Design and implement a simple baseline bandit algorithms. Benchmark it on
artificial data and Economia dataset (recommendation of news and articles).
Evaluate the performance of contextual bandit algorithm and explain relations
to baseline performance.

## Database Dump Restore

To restore a database dump $DBDUMP into a PostgreSQL $DATABASE issue:

	$ createdb -T template0 $DATABASE
	$ pg_restore -O -d $DATABASE $DBDUMP

## Related Work

For preprorocessing and modeling code sample see repository for
[News Article Recommendation](https://bitbucket.org/ondrejba/drl_recom).

## References

- [A Contextual-Bandit Approach to Personalized News Article Recommendation](https://arxiv.org/abs/1003.0146)
- [Unbiased Offline Evaluation of Contextual-bandit-based News Article Recommendation Algorithms](https://arxiv.org/abs/1003.5956)
- [The Epoch-Greedy Algorithm for Contextual Multi-armed Bandits](http://hunch.net/~jl/projects/interactive/sidebandits/bandit.pdf)
- [Explore/Exploit Schemes for Web Content Optimization](http://ieeexplore.ieee.org/document/5360225/)
- [Reinforcement Learning: An Introduction](http://incompleteideas.net/book/the-book-2nd.html)

## License for Sponsor and University

All results of this assignment are student's property and university
and sponsor has a license to use. The way of publication must respect NDA.
