Economia RL Package
===================

Code for experiments with reinforcement learning in Economia database.

Tests
-----

To test the package's code run:

.. code::

        $ python setup.py test
