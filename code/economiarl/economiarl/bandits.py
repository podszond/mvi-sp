import numpy
from functools import partial
from .preprocessing import get_user_latest_view


class RandomBandit:
    """Bandit which select items in random"""
    def __init__(self, item_pool):
        self.item_pool = item_pool

    def __call__(self, user, time):
        """Select a random action."""
        return numpy.random.choice(self.item_pool)
    
    def update(self, item, R):
        pass


class EpsilonGreedyBandit:
    """Bandit which estimated excpeted reward for each action
    and with probability epsilon selects a random item.i
    """
    def __init__(self, epsilon, item_pool):
        self.epsilon = epsilon
        self.item_pool = item_pool
        n_items = item_pool.size
        self.Q = numpy.zeros(n_items)  # estimate of action values
        self.N = numpy.zeros(n_items, dtype=numpy.int)  # action selections
    
    def __call__(self, user, time):
        """Select an action."""
        if numpy.random.rand() < 1 - self.epsilon:
            # arbitrary break ties
            max_Q = numpy.max(self.Q)
            max_idxs = numpy.argwhere(self.Q == max_Q).flatten()
            rnd_max_idx = numpy.random.choice(max_idxs)
            return self.item_pool[rnd_max_idx]
        else:
            return numpy.random.choice(self.item_pool)
    
    def update(self, item, R):
        """Update estimated action's value."""
        A = numpy.argwhere(self.item_pool == item)[0, 0]
        self.N[A] += 1
        self.Q[A] += (R - self.Q[A]) / self.N[A]



def get_item_features(itemid, items):
    return items.loc[itemid].as_matrix()


def get_user_features(userid, timestamp, items, views):
    itemid = get_user_latest_view(userid, timestamp, views)
    return get_item_features(itemid, items)


class DisjointLinUCB:
    def __init__(self, alpha, items, views, n_features, get_user_features=get_user_features):
        # how much to be sure with estimated upper bound
        self.alpha = alpha
        # d-dimensinal features of arms
        self.d = n_features
        # pandas DataFrame of all arms (here items)
        self.items = items
        # number of arms
        self.n_items = items.shape[0]
        # initially d-dimensional identity matrix for each arms
        # assumption is that the item pool is static
        self.A = numpy.repeat(numpy.eye(self.d)[numpy.newaxis, :, :], self.n_items, axis=0)
        # cache inversions intialized to eye matrix as it is a inversion of eye matrix
        self.A_inv = numpy.repeat(numpy.eye(self.d)[numpy.newaxis, :, :], self.n_items, axis=0)
        # d-dimensional zero column vector
        self.b = numpy.zeros((self.n_items, self.d, 1))
        # vector for arms' selection upper bounds
        self.p_t = numpy.zeros(self.n_items)
        # user feature extractor
        self.get_user_features = partial(get_user_features, items=items, views=views)
        # cached user features and item index
        self.user_features = None
        self.item_idx = None

    def __call__(self, userid, timestamp):
        # if user's features are missing recommend randomly
        self.user_features = self.get_user_features(userid, timestamp)
        # compute upper bound for selection of all arms
        theta = self.A_inv @ self.b
        x = (self.user_features - self.items.as_matrix()).reshape(self.n_items, self.d, 1)
        self.p_t = numpy.transpose(theta, axes=(0, 2, 1)) @ x + self.alpha * numpy.sqrt(numpy.transpose(x, (0, 2, 1)) @ self.A_inv @ x)
        # break ties arbitrary
        max_idxs = numpy.argwhere(self.p_t == numpy.max(self.p_t)).flatten()
        self.item_idx = numpy.random.choice(max_idxs)
        # return the arm with highest upper bound
        return self.items.iloc[self.item_idx].name

    def update(self, itemid, R):
        x_ta = self.user_features - self.items.iloc[self.item_idx].as_matrix()
        self.A[self.item_idx] += x_ta @ x_ta.T
        self.b[self.item_idx] += (R * x_ta).reshape(self.d, 1)
        self.A_inv[self.item_idx] = numpy.linalg.inv(self.A[self.item_idx])
