import psycopg2
import json
import pandas
import os


def execute_query(connection, query):
    """Generate result of the query.

    connection is a connection to PosgreSQL Economia database,
    query is a SQL query to execute.
    """
    with connection.cursor() as cursor:  # create a cursor
        cursor.execute(query)   # execute the query
        for record in cursor:
            yield record    # yield each record


def maybe_query(filename, query_fn, **kwarg):
    """Maybe execute the query_fn to query a database.

    name is the prefix of file where to cache the result,
    query_fn is query function executed if data are not cached.
    """
    if not os.path.isfile(filename):    # check if data are cached
        df = query_fn(**kwarg)
        df.to_csv(filename, index=False)    # write the DataFrame to the file
    else:   # else return the DataFrame saved in CSV
        print('INFO: using cached file:', filename)
        df = pandas.read_csv(filename)
    return df


def query_recomms(connection, from_date, to_date):
    """Query a database for recommendation between from_date and to_date."""
    # consider only dymamiclead scenario
    query = "SELECT userid, recomms, timestamp" \
            " FROM economia.recomms" \
            " WHERE scenario = 'dynamiclead'" \
            " AND DATE(timestamp) >= '{}'" \
            " AND DATE(timestamp) <= '{}'" \
            " ORDER BY timestamp ASC".format(from_date, to_date)
    recomms = list(execute_query(connection, query))
    # create pandas DataFrame
    recomms_df = pandas.DataFrame(
            # get only the first recommendation which is the major article
            ((userid, recommendations[0], time)
                for userid, recommendations, time in recomms
                # make sure that all has 6 recommendations
                # first is the major article
                # rest are minor articles
                if len(recommendations) == 6),
            columns=['userid', 'itemid', 'timestamp']
            )
    return recomms_df


def maybe_query_recomms(connection, from_date, to_date):
    # construct the filename
    filename = 'data/recomms-{}-{}.csv'.format(from_date, to_date)
    recomms_df = maybe_query(
            filename, query_fn=query_recomms,
            connection=connection, from_date=from_date, to_date=to_date,
            )
    recomms_df['timestamp'] = pandas.to_datetime(recomms_df['timestamp'])
    return recomms_df


def query_item_features(connection, itemid):
    """Return item's features."""
    # get the LSI features
    column = 'lsi_tags_json'
    query = "SELECT {} FROM economia.items WHERE itemid = '{}'".format(
            column, itemid
            )
    # should be only one row so extract it
    raw_features = list(execute_query(connection, query))[0]
    if isinstance(raw_features[0], str):
        return json.loads(raw_features[0])  # load the features from a string
    return dict()   # on error return empty dictionary


def query_items(connection, views_df):
    """Query items' features."""
    items_features = list()
    for item in views_df['itemid'].unique():
        features = query_item_features(connection, item)
        if len(features) != 200:    # sanity check
            continue
        # sort features
        sorted_features = sorted(features.items(), key=lambda x: x[0])
        items_features.append([item] + [v for k, v in sorted_features])
    return pandas.DataFrame(
            items_features,
            columns=['itemid'] + list(range(200))
            )


def maybe_query_items(connection, views_df, from_date, to_date):
    # construct the filename
    filename = 'data/items-{}-{}.csv'.format(from_date, to_date)
    return maybe_query(
            filename, query_fn=query_items,
            connection=connection, views_df=views_df
            )


def query_views(connection, from_date, to_date):
    """Query history of views."""
    query = "SELECT userid, itemid, timestamp" \
            " FROM economia.detail_views" \
            " WHERE DATE(timestamp) >= '{}' AND DATE(timestamp) <= '{}'" \
            " ORDER BY timestamp ASC".format(from_date, to_date)
    views = list(execute_query(connection, query))
    return pandas.DataFrame(views, columns=['userid', 'itemid', 'timestamp'])


def maybe_query_views(connection, from_date, to_date):
    filename = 'data/views-{}-{}.csv'.format(from_date, to_date)
    views_df = maybe_query(
            filename, query_fn=query_views,
            connection=connection, from_date=from_date, to_date=to_date
            )
    views_df['timestamp'] = pandas.to_datetime(views_df['timestamp'])
    return views_df


def get_data(database, user, from_date, to_date):
    """Get recommendation data between from_date and to_date.

    database is a name of a PostgreSQL Economia database.
    user is a name of the owner of database.

    Return DataFrames of recommendations, items and views history.
    """
    # get base data
    with psycopg2.connect(database=database, user=user) as connection:
        print('INFO: getting recommendations')
        recomm_df = maybe_query_recomms(connection, from_date, to_date)
        print('INFO: getting views')
        view_df = maybe_query_views(connection, from_date, to_date)
        print('INFO: getting items')
        item_df = maybe_query_items(connection, view_df, from_date, to_date)
    print('INFO: processing DataFrames')
    # left outer join on recommendation to find out which where sucessfull
    recomm_df = pandas.merge(
            recomm_df, view_df, how='left', on=['userid', 'itemid'],
            suffixes=('', '_view'), sort=False
            )
    # sort according to recommendation time
    # drop duplicated recommendation
    # TODO this is probably wrong
    recomm_df.sort_values('timestamp', inplace=True)
    recomm_df.drop_duplicates(['userid', 'itemid'], keep='last', inplace=True)
    # calculate the time difference between recommendation and view
    recomm_df['timedelta'] = recomm_df['timestamp_view'] - recomm_df['timestamp']
    # initialize reward to 1
    recomm_df['reward'] = 1
    # create index for unsuccesfull recommendations
    index = recomm_df['timedelta'] > pandas.Timedelta('10 minutes')
    index |= recomm_df['timedelta'] <= pandas.Timedelta('0 minutes')
    index |= recomm_df['timedelta'].isnull()
    # set reward to 0 for unsuccesfull recommendations
    recomm_df.loc[index, 'reward'] = 0
    # drop irrelevant columns
    recomm_df.drop(['timestamp_view', 'timedelta'], axis=1, inplace=True)

    # left only those users where reward information is available
    users_poll = recomm_df['userid'].unique()
    # left only those items which where viewed
    items_poll = view_df['itemid'].unique()
    view_df = view_df[view_df['userid'].isin(users_poll)]
    item_df = item_df[item_df['itemid'].isin(items_poll)]

    # TODO this should be done more carefully
    view_df.drop_duplicates(['userid', 'itemid'], keep='first', inplace=True)
    # group history by users
    view_df.index.name = 'id'
    view_df = view_df.sort_values('userid').reset_index(level=0).set_index(['userid', 'id'])

    item_df.set_index('itemid', inplace=True)

    return recomm_df, item_df, view_df
