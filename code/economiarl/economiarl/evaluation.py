import numpy
from tqdm import tqdm


def policy_evaluator(bandit, recomms):
    """Evaluate a bandit algorithm with finite data stream.
    
    bandit is a bandit algorithm,
    recomms is a DataFrame with recommendtaions.
    
    Refer to paper on arxiv: https://arxiv.org/abs/1003.5956.
    """
    G = 0  # initially zero total payoff
    T = 0  # initially zero counter of valid events
    n_recomms = recomms.shape[0]
    for idx in tqdm(range(n_recomms)):
        userid = recomms.iloc[idx]['userid']
        timestamp = recomms.iloc[idx]['timestamp']
        itemid = recomms.iloc[idx]['itemid']
        reward = recomms.iloc[idx]['reward']
        if bandit(userid, timestamp) == itemid:
            bandit.update(itemid, reward)
            G += reward
            T += 1
    if T == 0:
        return 0, 0
    return G / T, T


def evaluate_bandit(bandit, recomms, n_evals=30):
    """Evaluate a bandit n_evals time to get standart deviation."""
    avg_returns = numpy.zeros(n_evals)
    Ts = numpy.zeros(n_evals)
    for i in range(n_evals):
        avg_returns[i], Ts[i] = policy_evaluator(bandit, recomms)
    return avg_returns, Ts
