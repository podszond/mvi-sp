from functools import partial


def get_user_history(userid, views):
    return views.loc[userid]


def get_user_latest_view(userid, timestamp, views):
    history = get_user_history(userid, views)
    return history[history['timestamp'] < timestamp].iloc[-1]['itemid']


def recomms_with_history(row, views):
    try:
        get_user_latest_view(row['userid'], row['timestamp'], views)
        return True
    except (KeyError, IndexError):
        return False


def clean_data(n_items, recomms, items, views):
    # limit to smaller item pool
    item_pool = recomms['itemid'].value_counts().nlargest(n_items).index.values
    items = items[items.index.isin(item_pool)]
    views = views[views['itemid'].isin(item_pool)]
    recomms = recomms[recomms['itemid'].isin(item_pool)]
    # leave only recommendation with user features available
    recomms = recomms[recomms.apply(
        partial(recomms_with_history, views=views),
        axis='columns'
    )]
    return recomms, items, views
