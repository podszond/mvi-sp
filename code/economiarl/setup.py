from setuptools import setup, find_packages


setup(
        name='economiarl',
        version='0.0.0',
        description='Reinforcement learning in Economia database.',
        author='Ondřej Podsztavek',
        author_email='podszond@fit.cvut.cz',
        packages=find_packages(),
        setup_requires=['pytest-runner'],
        tests_require=['pytest'],
        )
