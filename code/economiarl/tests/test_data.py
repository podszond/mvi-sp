import pytest
import psycopg2
from economiarl import db


@pytest.fixture
def connection():
    db = 'economiadb'
    user = 'podsztavek'
    with psycopg2.connect(database=db, user=user) as connection:
        yield connection


def test_user_history_len(connection):
    """Test user history length.

    SELECT COUNT(*) FROM economia.detail_views WHERE userid = 'Vk-Rkh9F57';
     count
    -------
       211
    (1 row)
    """
    user_history = list(db.gen_user_history(connection, 'Vk-Rkh9F57'))
    assert len(user_history) == 211


def test_get_items_features(connection):
    # TODO extend
    item_features = db.get_item_features(connection, 'caf5b9fcace811e7811f002590604f2e')
    assert isinstance(item_features, dict)
