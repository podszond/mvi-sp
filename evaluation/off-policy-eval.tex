\documentclass{beamer}
\beamertemplatenavigationsymbolsempty
\usepackage{hyperref}
\usepackage{algorithm2e}
\usepackage{algorithmic}
\usepackage{biblatex}
\addbibresource{off-policy-eval.bib}

\title{Reinforcement Learning in Recommendation}
\subtitle{Off-policy Policy Evaluation}
\author{Ond\v{r}ej Podsztavek}
\institute{
	Faculty of Information Technology\\
	Czech Technical University in Prague
	}
\date{\today}
\subject{Reinforcement Learning}

\begin{document}
\maketitle
\frame{
	\frametitle{Outline}
	\tableofcontents
	}
\section{Introduction}
\frame{
	\frametitle{Motivation}
	\begin{itemize}
		\item Recommendation algorithms are typically optimized for
			click through rate:
			$$\mathit{CTR} \stackrel{\text{def}}{=}
			\frac{\text{total number or clicks}}
			{\text{total number of \textbf{visits}}} \times 100.$$
		\item But there is interest in optimizing customer lifetime
			value:
			$$\mathit{LTV} \stackrel{\text{def}}{=}
			\frac{\text{total number or clicks}}
			{\text{total number of \textbf{visitors}}} \times 100.$$
		\item Reinforcement learning can possibly address this problem.
	\end{itemize}
	}
\frame{
	\frametitle{Goals}
	\begin{enumerate}
		\item Design off-line performance metric.
		\item Implement simulator to measure performance.
		\item Measure performance of context-free bandit as simple
			baseline.
		\item Move on to contextual bandits and full reinforcement
			learning algorithms.
	\end{enumerate}
	}
\frame{
	\frametitle{Challenges}
	How to \textbf{compute good lifetime value strategy} and
	\textbf{evaluate} it off-line (\textbf{off-policy}) from logged data
	collected by different (\textbf{behavior}) policy.
	}
\section{Contextual Bandits}
\frame{
	\frametitle{Contextual Bandits}
	\begin{itemize}
		\item Select articles to serve users based on contextual
			information about users and articles.
		\item Simultaneously adapting its selection strategy according
			to user-click feedback.
		\item Maximize $\mathit{CTR}$.
	\end{itemize}
	}
\frame{
	\frametitle{Feature-based Exploration and Exploitation Problem}
	\begin{itemize}
		\item Large number users and content represented by features.
		\item Critical to \textbf{generalize} users and content.
		\item Balance user satisfaction in long run
			(\textit{exploitation}) and gathering information about
			goodness of content (\textit{exploration}).
	\end{itemize}
	\begin{definition}
		Contextual bandit algorithm $\mathbf{A}$ proceeds in time steps
		$t = 1, 2, 3, \dots$ and at each $t$:
		\begin{enumerate}
			\item $\mathbf{A}$ observes a user $u_t$ and
				a set $\mathcal{A}_t$ of actions
				characterized by \textit{context} vector
				$\mathbf{x}_{t, a}$ summarizing both the user
				$u_t$ and the action $a$.
			\item $\mathbf{A}$ chooses and action $a_t$ and
				receives reward $r_{t, a_t}$.
			\item $\mathbf{A}$ improves its selection
				strategy with the new observation
				$(\mathbf{x}_{t, a}, a_t, r_{t, a_t})$
		\end{enumerate}
	\end{definition}
	}
\frame{
	\frametitle{Total T-trial Return}
	\textit{Total T-trial return} is defined as:
	$$G(T) \stackrel{\text{def}}{=} \sum^{T}_{t = 1} r_{t, a_t}$$
	and \textit{optimal expected T-trial return}:
	$$G^*(T) \stackrel{\text{def}}{=} \mathbf{E}\left[\sum^{T}_{t = 1}
	r_{t, a^*_t}\right].$$
	In context of article recommendation:
	\begin{itemize}
		\item An article is an action.
		\item If article is clicked on the reward is 1 else 0
			then expected return is $\mathit{CTR}$.
	\end{itemize}
	}
\frame{
	\frametitle{LinUCB~\footfullcite{li2010}, Linear Upper Confidence Bound}
	Estimate mean reward of $\hat{\mu}_{t, a}$ and confidence interval
	$c_{t, a}$, such that with high probability:
	$$|\hat{\mu}_{t, a} - \mu_a| < c_{t, a}, \quad a_t =
	\operatorname{arg\,max}_a (\hat{\mu}_{t, a} + c_{t, a}).$$
	LinUCB with \textit{disjoint} linear models:
	$$\mathbf{E}[r_{t, a} | \mathbf{x}_{t, a}] = \mathbf{x}_{t, a}^{\top}
	\mathbf{\theta}^*_a.$$
	LinUCB with \textit{hybrid} linear models:
	$$\mathbf{E}[r_{t, a} | \mathbf{x}_{t, a}] =
	\mathbf{z}_{t, a}^{\top} \mathbf{\beta}^*
	+ \mathbf{x}_{t, a}^{\top} \mathbf{\theta}^*_a.$$
	Both learned with \textit{ridge regression}.
	}
\frame{
	\frametitle{Approaches Off-line Evaluation}
	\begin{enumerate}
		\item \textbf{On-line} evaluation \textit{expensive} and
			\textit{not reproducible}.
		\item \textbf{Simulator} is challenging to implement moreover
			might be biased.
		\item \textbf{Off-line} data could provide unbiased estimate
			but they are \textit{partially-labeled} (only one
			action has reward feedback).
	\end{enumerate}
	}
\frame{
	\frametitle{Off-line Evaluation of Contextual
	Bandits~\footfullcite{li2012}}
	\begin{algorithm}[H]
	\begin{algorithmic}[1]
		\STATE $h_0 \leftarrow \emptyset$, $\hat{G}_\mathbf{A}
		\leftarrow 0$, $T \leftarrow 0$
		\FOR{$t = 1, 2, 3, \dots, L$}
		\STATE get the $t$-th event $(\mathbf{x}, a, r_a)$ from $S$
		\COMMENT{stream $S$ of length $L$}
		\IF{$\mathbf{A}(h_{t - 1},\mathbf{x}) = a$}
		\STATE $h_t \leftarrow$ concatenate($h_{t-1},
		(\mathbf{x}, a, r_{a})$)
		\STATE $\hat{G}_\mathbf{A} \leftarrow \hat{G}_\mathbf{A} +
		r_{a}$, $T \leftarrow T + 1$
		\ELSE
		\STATE $h_t \leftarrow h_{t - 1}$
		\ENDIF
		\ENDFOR
		\RETURN $\hat{G}_\mathbf{A} / T$
	\end{algorithmic}
	\end{algorithm}
	\begin{block}{Assumptions}
		Stable arms set. Logging policy chooses arms uniformly at
		random.  Data, events are IID.
	\end{block}
	}
\frame{
	\frametitle{Direct Method}
	Estimate the value of policy $\pi$ (policy evaluation):
	$$V^{\pi} = \mathbf{E}_{(x, \mathbf{r}) \sim D}[r_{\pi(x)} | x].$$
	Policy optimization is to find an optimal policy with maximum value:
	$$\pi^* = \operatorname{arg\,max}_\pi V^\pi.$$
	Form an estimate of
	$\hat{\rho}_a(x) = \mathbf{E}_{(x, \mathbf{r}) \sim D}[r_a | x]$
	of expected reward considering a context and an action:
	$$\hat{V}^{\pi}_{\text{DM}} = \frac{1}{|S|} \sum_{x \in S}
	\hat{\rho}_{\pi(x)}(x).$$
	Estimate $\hat{\rho}$ might be biased (is based on different policy).
	}
\frame{
	\frametitle{Inverse Propensity Score}
	Approximate behavior policy $\hat{p}(a | x)$ of $p(a | x)$ and
	correct the proportion between target and behavior policy:
	$$\hat{V}^{\pi}_{\text{IPS}} = \frac{1}{|S|} \sum_{(x, a, r_a) \in S}
	\frac{r_a \mathbf{I}(\pi(x) = a)}{\hat{p}(a | x)}$$
	In practice no problem with bias but high variance.
	}
\frame{
	\frametitle{Doubly Robust Estimator~\footfullcite{dudik2011}}
	Take advantage of both \textit{direct model} and \textit{inverse
	propensity score}:
	$$\hat{V}^{\pi}_{\text{DR}} = \frac{1}{|S|} \sum_{(x, a, r_a) \in S}
	\left[ \frac{(r_a - \hat{\rho}_a(x))
	\mathbf{I}(\pi(x) = a)}{\hat{p}(a | x)} +
	\hat{\rho}_{\pi(x)}(x)\right].$$
	Intuition is to use $\hat\rho$ as a baseline and if data available
	apply correction.
	}
\section{Full Reinforcement Learning}
\frame{
	\frametitle{Full Reinforcement Learning}
	\textit{Reinforcement learning algorithms} distinguish between
	a visit and a visitor.
	Moreover, they can \textit{learn from delayed reward}.
	\begin{block}{Motivation}
	\textit{"I expected to find something in recommendation systems, but I
	believe those are still dominated by collaborative filtering and
	contextual bandits. (...) Every Internet company ever has probably
	thought about adding RL to their ad-serving model, but if anyone’s
	done it, they've kept quiet about it."}~\footfullcite{irpan2018}
	\end{block}
	\begin{block}{Advantages over contextual bandits}
		Sufficient if users establish long-term relationships
		by returning back (do not expect i.i.d. visits).
	\end{block}
	}
\frame{
	\frametitle{Markov Decision Process}
	\begin{definition}
		MDP is a tuple $\mathcal{M} = (\mathcal{S}, \mathcal{A},
		\mathcal{P}, \mathcal{R}, \gamma)$, where $\mathcal{S}$
		is a set of possible states, $\mathcal{A}$ is a finite set of
		actions, $\mathcal{P}(s, a, s')$ is a probability of transition
		to $s'$ when action $a$ is taken in state $s$,
		$\mathcal{R}(s, a) \in \mathbb{R}$ is a reward received when
		action $a$ is taken in state $s$ and $\gamma \in [0, 1]$ is
		a discount factor.
	\end{definition}
	In recommendation context:
	\begin{itemize}
		\item $\mathcal{S}$ is set of feature vectors describing a
			user,
		\item $\mathcal{A}$ is set of articles to recommend,
		\item $\mathcal{P}$ described (\textbf{unknown}) dynamics of
			users and
		\item $\mathcal{R}(s, a)$ is 1 if a user click on the article
			$a$ else 0.
	\end{itemize}
	}
\frame{
	\frametitle{Reinforcement Learning Objective}
	\begin{block}{Goal}
		Find a decision rule called \textit{policy} $\pi$ that maximizes
		the expected performance $\mathbf{E}[R(\tau)|\pi]$.
	\end{block}
	\begin{itemize}
		\item Policy $\pi(a | s)$ denotes the probability of taking
			action $a$ in state $s$.
		\item Episode produces a \textit{trajectory}
			$\tau = {s_1, a_1, r_1, \dots, s_T, a_T, r_T}$.
		\item $T$ is a time horizon.
		\item $R(\tau) = \sum^T_{t = 1} \gamma^{t - 1} r_t$ is the
			\textit{return} of trajectory $\tau$.
	\end{itemize}
	}
\frame{
	\frametitle{Off-line Evaluation in Full Reinforcement Learning}
	\begin{enumerate}
		\item \textbf{Simulator-based}: Fit a MDP model from the data
			and evaluate the against model.
		\item \textbf{Simulator-free}: Evaluate based on
			\textit{importance sampling} which correct the mismatch
			between target and behavior policy.
	\end{enumerate}
	}
\frame{
	\frametitle{Importance Sampling~\footfullcite{precup2000}}
	Estimate the expected value of a random variable $x$ with distribution
	$d$ from sample drawn from distribution $d'$:
	$$\mathbf{E}_d[x] = \int x d(x) dx = \int x \frac{d(x)}{d'(x)} d'(x)
	dx = \mathbf{E}_{d'}\left[x \frac{d(x)}{d'(x)}\right].$$
	\textit{Unbiased} and \textit{consistent} estimate:
	$$\frac{1}{n} \sum_{i = 1}^n x_i \frac{d(x_i)}{d'(x_i)}.$$
	}
\frame{
	\frametitle{Importance Sampling Estimator}
	Provide unbiased estimate of $\pi_b$ value.
	\\
	Define the per-step importance ratio:
	$$\rho_t \stackrel{\text{def}}{=}
	\frac{\pi_t(a_t | s_t)}{\pi_b(a_t | s_t)}$$
	and cumulative importance ratio:
	$$\rho_{1:t} \stackrel{\text{def}}{=} \prod_{t' = 1}^t \rho_{t'}.$$
	Trajectory-wise \textit{importance sampling} estimate:
	$$V_{\text{IS}} \stackrel{\text{def}}{=} \sum_{t = 1}^H \gamma^{t - 1}
	\rho_{1:t} r_t.$$
	}
\frame{
	\frametitle{
		High Confidence Off-policy
		Evaluation~\footfullcite{theocharous2015}
		}
	\textit{Model-free} approach to off-policy evaluation. Compute lower
	bound on true performance $\mathbf{E}[R(\tau)|\pi]$ using
	\textit{importance sampling}.
	Three approaches:
	\begin{itemize}
		\item concentration inequality~\footfullcite{thomas2015},
		\item Student's $t$-test,
		\item bias corrected and accelerated bootstrap.
	\end{itemize}
	Suitable for safe policy improvement.
	}
\frame{
	\frametitle{Doubly Robust Off-policy Value
	Evaluation~\footfullcite{jiang2015}}
	\textit{Doubly robust estimator} for sequential decision-making.
	Unbiased and much lower variance than \textit{importance sampling}.
	\begin{block}{MAGIC~\footfullcite{thomas2016}}
		Better extension of doubly robust estimator.
	\end{block}
	}
\section*{Futuristic Vision}
\frame{
	\frametitle{Futuristic Vision}
	Model-based reinforcement learning which while recommending models
	users and based on its model plans what to recommend.
	}
\end{document}
