\documentclass{mvi-report}
\usepackage[utf8]{inputenc} 

\title{Reinforcement Learning in Recommendation}
\author{Ondřej Podsztavek}
\affiliation{Faculty of Information Technology, CTU in Prague}
\email{podszond@fit.cvut.cz}

\def\file#1{{\tt#1}}

\begin{document}
\maketitle

\section{Introduction}

Recommendation algorithms are typically optimized for the conversion rate.
However many companies are interested in optimizing customer lifetime value
or engagement directly.
Reinforcement learning in recommendation can possibly address this
problem.~\cite{sutton17}

This work aims to design and implement a simple baseline bandit algorithms.
Then, benchmark it on artificial data and Economia dataset
(recommendation of news and articles).
Finally, evaluate the performance of contextual bandit algorithm
and explain relations to baseline performance.

\section{Data}
\label{sec:data}

This work focuses on news article recommendation data from \url{centrum.cz}.
For simplicity it is focused only on recommendation of the major article
and not the minor 5 articles (see figure~\ref{fig:centrum}).

\begin{figure}[ht]
  \centering
  \leavevmode
  \includegraphics[width=\linewidth]{../code/img/centrum.png}
  \vskip-0.5cm
  \caption{
    Slate of one major and five minor recommended articles
    on \url{centrum.cz}
  }
  \label{fig:centrum}
\end{figure}

Furthermore, experiment done in this work use data from 7 days
(1st--7th August 2017).
During this period there were 2809326 recommendations of the major article.
1140 recommended unique items were recommended
and a recommendation was generated for 1659973 different users.
From these users 1445679 have only 1 recommendation.

In addition to recommendation information, there are only user view history.
The view history has 4002693 entries and 165880 unique users.
The number of unique users is much smaller than the number of users
with recommendation due to the fact that most user visit \url{centrum.cz}
to view their mail account or to issue a web query and not to read news.
Viewed items were 11149 which means that there is no recommendation information
about most of items relevant in the time window.

Using both recommendations and view history, it is possible to get successful
recommendation information.
It was chosen that a recommendation if a user viewed an item 10 minutes
after the item was recommended to her or him.
From all recommendations only 2.5\% are successful and only 47255 users
has at least one successful recommendation.

\begin{figure}[ht]
  \centering
  \leavevmode
  \includegraphics[width=\linewidth]{../code/img/success-ratio.pdf}
  \vskip-0.5cm
  \caption{Only 2.5\% of all recommendation are successful.}
  \label{fig:success-ratio}
\end{figure}

\section{Methods}
\label{sec:methods}

This work aims to use a contextual algorithms to tackle the problem
of recommendation.
Here LinUCB\cite{li10b} contextual bandit algorithm is used
and is compared against two baseline algorithms, namely random and
$\varepsilon$-greedy bandit algorithms.
In order to evaluate the methods offline evaluation is taken from~\cite{li10a}.

Random bandit algorithm is a simple method which at each time select
an action uniformly at random from a items pool.
Thus benchmarking against this bandit should assure
that recommendations are meaningful.
$\varepsilon$-greedy bandit estimated value of each item and at each time
select the action with the highest estimated value but with probability
$\varepsilon$ select an action uniformly at random.\cite{sutton17}

LinUCB method presented in~\cite{li10b} is a contextual bandit
which uses features of both users and articles together with
ridge regression to learn optimal article selection strategy.

\section{Experiments and Results}

All methods described in section~\ref{sec:methods} were evaluated on data
from section~\ref{sec:data} and the evaluation metric was the
policy evaluator algorithm with finite data stream proposed in~\cite{li10a}.

The figure~\ref{fig:random} show average return per recommendation trial
of a random bandit together with behavioral policy performance.
The behavioral policy is the algorithm used to while gathering the data
and its average return is computed as the ration of successful recommendation
to the total number of recommendations.
It might seem that the behavioral policy is also a random bandit but this
might be caused by the fact the behavioral policy chooses from much larger pool
of items than the random policy which chooses only from items were
some recommendation information is available.
It is still interesting to compare to the performance of behavioral policy
so it will be also shown in other figures.

\begin{figure}[ht]
  \centering\leavevmode
  \includegraphics[width=\linewidth]{../code/img/random-bandit.pdf}
  \vskip-0.5cm
  \caption{
    Average return of a random bandit algorithm shown as box plot and
    compared to estimated performance of behavioral policy.
    The box plot is produced by 30 evaluation measurements.
  }
  \label{fig:random}
\end{figure}

The $\varepsilon$-greedy bandit algorithm has a parameter $\varepsilon$ which
has to be set by a user. There in this work it was evaluated for values
shown in the figure~\ref{fig:epsilon}.
The figure shows that this bandit is able to learn something if $\varepsilon$
parameter is sufficiently small but as it is higher that 0.7 that its
performance is close to the performance of random bandit.

\begin{figure}[ht]
  \centering\leavevmode
  \includegraphics[width=\linewidth]{../code/img/epsilon-greedy-bandit-0.pdf}
  \vskip-0.5cm
  \caption{
    Performance of $\varepsilon$-greedy bandit algorithms for different
    values of $\varepsilon$. The figure shows that for some algorithm's setting
    the agent is able to produce much better results that behavioral policy.
    Each box plot is produced by 30 evaluation measurements.
  }
  \label{fig:epsilon}
\end{figure}

Concerning the LinUCB evaluation, the algorithm shown
that it is terrible inefficient in comparison to the other two bandits
and it would take a week to do one evaluation on all data.
Therefore top 10 most often recommended items where selected
reducing the data to 41076 recommendations and 52825 views in history.
In this setting the evaluation is tractable.

Parameter delta was selected as 0.05 giving 95\% probability that the
upper bound estimated by LinUCB will hold.
An article's features were 200 latent semantic indexes
and a user's features were latent semantic indexes of last article read
before the recommendation being evaluated.

The result compared to baseline performance are presented
in figure~\ref{fig:linucb}.
Note that there is no deviation in LinUCB performance
as the algorithm is almost deterministic (only breaks ties randomly)
in contrast to random and $\varepsilon$-greedy bandits.
The figure shows that while LinUCB cannot beat the greedy bandit
and it is almost random it is still better then a random bandit.
Maybe better feature extraction would improve its performance
but there is still the efficiency problem.

\begin{figure}[ht]
  \centering\leavevmode
  \includegraphics[width=\linewidth]{../code/img/linucb.pdf}
  \vskip-0.5cm
  \caption{
    Performance of LinUCB in comparison to baseline performance.
    Each box plot is produced by 10 evaluation measurements.
  }
  \label{fig:linucb}
\end{figure}

\section{Discussion}

This work shows that reinforcement learning in recommendation is hard.
Firstly, evaluation must be done offline
because online evaluation is too expensive.
Secondly, an algorithm need to be very fast as the selection
must be delivered to a user as fast as possible.

From the three methods presented in this work
the only useful in article recommendation is the $\varepsilon$-greedy method.
Random bandit shows too bad performance
and LinUCB is very slow
but with better feature extraction might give better results.

Furthermore, more sample efficient evaluation metric
as direct method, inverse propensity score or
double robust policy evaluation~\cite{dudik11} is needed.
Metric used in this work supposes use of random data
which were not available thus the result has high variance.
Other improvement would be to use top-$n$ metric instead of direct hit
but the metrics are usually theoretically constructed for only top-1.

\section{Related work}

\cite{li10b} introduces disjoint and hybrid variants of LinUCB algorithm
which is an instance of contextual bandit algorithm.
The LinUCB algorithm uses ridge regression to fit its parameters together
with upper confidence bound (UCB) scheme as exploration mechanism.
Moreover, the work proposes offline evaluation method.
The algorithms are evaluated on news article recommendation
and are compared to context-free methods as random and $\varepsilon$-greedy.
The experiments show 12.5\% performance lift on LinUCB compared to the
context-free methods.

\cite{wang17} presents binary upper confidence bound (BiUCB) algorithm
which is mainly focused on cold-start situations
(no prior information about users our items)
and diversification of recommendation.
The algorithm is based on the idea of the LinUCB but its improvement is that
it selects both the most suitable user for an item
and also the other way around.
Performance is evaluated on MovieLens\footnote{\url{https://movielens.org/}}
using precision, diversity and novelty metrics.
With respect to these metrics, the algorithm is shown to be better than LinUCB.

\cite{tang15} proposes a parameter-free strategy for contextual bandit
problems which employs a probability matching approach called online bootstrap.
The work primarily wants to address the cold-start problem.
The proposed algorithm is evaluated on online advertising and news
recommendation using algorithm formed in \cite{li10a}.
The results are better than the performance of LinUCB
but the margin is very small.

\cite{zeng16} points out and describes the problem of dynamically changing
reward mapping function. Its described method uses dynamical context drift
model based on particle learning. The method can be integrated with
for example LinUCB leading to performance improvement by dynamically
tracking the reward model and thus increase the click-through rate.
The work uses the same evaluation methodology and datasets as \cite{tang15}.

\section{Conclusion}

This work presents results of simple reinforcement learning algorithm
in news articles recommendation.
It was shown that $\varepsilon$-greedy bandit can easily beat random
bandit. Moreover, this work suggest the LinUCB is not suitable for this problem
because it is terribly slow.
A lot of work has to be done also in the offline evaluation domain.
The simple method used here is very sample inefficient
and gives results with high variance.
All in all, the experience gathered during these work are very valuable
and should help to tackle the problem of reinforcement learning in recommendation
and offline evaluation of reinforcement learning algorithms in general.

\bibliography{reference}

\end{document}
